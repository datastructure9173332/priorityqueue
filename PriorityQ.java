public class PriorityQ {
    // Array in sorted order, from max at 0 to min at size-1
    private int maxSize;
    private long[] queArray;
    private int nItems;

    // Constructor
    public PriorityQ(int s) {
        maxSize = s;
        queArray = new long[maxSize];
        nItems = 0;
    }

    // Insert an item
    public void insert(long item) {
        int j;
        if (nItems == 0) {
            // If no items, insert at 0
            queArray[nItems++] = item;
        } else {
            // If items, insert while maintaining sorted order
            for (j = nItems - 1; j >= 0; j--) {
                if (item > queArray[j]) {
                    // If new item is larger, shift upward
                    queArray[j + 1] = queArray[j];
                } else {
                    // If smaller, done shifting
                    break;
                }
            }
            queArray[j + 1] = item; // Insert the item
            nItems++;
        }
    }

    // Remove the minimum item
    public long remove() {
        return queArray[--nItems];
    }

    // Peek at the minimum item
    public long peekMin() {
        return queArray[nItems - 1];
    }

    // Check if the queue is empty
    public boolean isEmpty() {
        return (nItems == 0);
    }

    // Check if the queue is full
    public boolean isFull() {
        return (nItems == maxSize);
    }
}